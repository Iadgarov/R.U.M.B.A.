/*
 * Score.h
 *
 *  Created on: 25 Mar 2016
 *      Author: Liat
 */

#ifndef SCORE_H_
#define SCORE_H_

class Score {

	int winner_num_steps;
	int sum_dirt_in_house;
	int pos_in_competition;
	int this_num_steps;
	int dirt_collected;
	bool is_back_in_docking;

	int finalScore;

public:

	Score() {}

	Score(int apic, int dc, int sdih, bool bid)
		: sum_dirt_in_house(sdih), dirt_collected(dc), is_back_in_docking(bid), finalScore(-1) {
		
		bool finished = (sum_dirt_in_house == dirt_collected);

		pos_in_competition = (!finished || apic <= 4 ? apic : 4);
		//finalScore = calculateScore();
			
	}

	// for badly behaving algorithms
	void setScoreZero() {
		finalScore = 0;
	}
	
	// set the needed params
	void setParams(int wns, int tns) {
		winner_num_steps = wns;
		this_num_steps = tns;
		
		finalScore = calculateScore();
		
		/*cout << "winner_num_steps: " <<  winner_num_steps << endl;
		cout << "sum_dirt_in_house: " << sum_dirt_in_house << endl;
		cout << "pos_in_competition: " << pos_in_competition << endl;
		cout << "this_num_steps: " << this_num_steps << endl;
		cout << "dirt_collected: " << dirt_collected << endl;
		cout << "is_back_in_docking: " << is_back_in_docking << endl;
		cout << "finalScore: " << finalScore << endl; */
	}

	int getScore() {
		return finalScore;
	}

private:

	int calculateScore() {
		int score = 2000
				- (pos_in_competition - 1) * 50
				+ (winner_num_steps - this_num_steps) * 10
				- (sum_dirt_in_house - dirt_collected) * 3
				+ (is_back_in_docking ? 50 : -200);

		return (score > 0 ? score : 0);

	}

};



#endif /* SCORE_H_ */
