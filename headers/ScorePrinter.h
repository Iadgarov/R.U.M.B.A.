#ifndef SCOREPRINTER_H_
#define SCOREPRINTER_H_

#include "Score.h"
#include <map>
#include <sstream>
#include <iomanip>
#include <string>
#include <iostream>

using namespace std;

class ScorePrinter {

	map<string, map<string, int>>& scoreMatrix; // a reference to a score matrix (house and alg names are already stripped of prefix/suffix)
	
	int cols, rows;
	string dashLine;
	
public:
	
	ScorePrinter(map<string, map<string, int>>& matrix) : scoreMatrix(matrix) {
		rows = matrix.size() + 1;
		cols = matrix.begin()->second.size() + 2;
		createDashes();
	}
	
	void print();
	
private:
	
	void createDashes() {
		int numOfSeparators = cols + 1; // number of '|' in each line
		int numOfDashes = cols * 10 + 3 + numOfSeparators;
		dashLine = string(numOfDashes, '-');
	}
	
	string alignRight(string name, size_t len);
	
	string alignLeft(string name, size_t len);


};

#endif