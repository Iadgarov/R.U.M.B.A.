#ifndef __AlGORITHM_REGISTRY__H_
#define __AlGORITHM_REGISTRY__H_

#include <iostream>
#include <string>
#include <vector>
#include "../interface/Direction.h"
#include "../interface/AbstractAlgorithm.h"

using namespace std;

template<class T> AbstractAlgorithm* algorithm_factory() {
    return new T;
}

typedef AbstractAlgorithm* (*algorithm_creator)(void);

class AlgorithmRegistry {

    private:
        vector<algorithm_creator> m_algorithms;

    public:
        typedef vector<algorithm_creator>::iterator iterator;

        static AlgorithmRegistry& get();

        void add(algorithm_creator);

        iterator begin();
        iterator end();
};

class AlgorithmRegistration {

public:
	AlgorithmRegistration(algorithm_creator);
};

#define AUTO_REGISTER_ALGORITHM(algorithm) \
		AlgorithmRegistration _algorithm_registration_ ## algorithm(&algorithm_factory<algorithm>);

#endif
