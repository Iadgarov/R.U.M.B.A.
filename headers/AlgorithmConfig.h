#ifndef __ALGORITHM_CONFIG__H_
#define __ALGORITHM_CONFIG__H_

#include <map>
#include <iostream>
using namespace std;

typedef struct AlgorithmConfig {
    int MaxStepsAfterWinner;
    int BatteryCapacity;
    int BatteryConsumptionRate;
    int BatteryRechargeRate;

	void setConfig(map<string, int> config) {
		MaxStepsAfterWinner = config.at("MaxStepsAfterWinner");
		BatteryCapacity = config.at("BatteryCapacity");
		BatteryConsumptionRate = config.at("BatteryConsumptionRate");
		BatteryRechargeRate = config.at("BatteryRechargeRate");
	}

} AlgConfig;

#endif //__ALGORITHM_CONFIG__H_
