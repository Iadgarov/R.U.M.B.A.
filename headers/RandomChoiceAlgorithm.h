/*
 * RandomChoiceAlgorithm.h
 *
 *  Created on: Mar 16, 2016
 *      Author: ubuntu
 */

#ifndef RANDOMCHOICEALGORITHM_H_
#define RANDOMCHOICEALGORITHM_H_

#include "AlgorithmConfig.h"
#include <iostream>
#include <map>
#include "Sensor.h"
#include <time.h>
#include <stdlib.h>
#include <stdio.h>

#include "../interface/AbstractAlgorithm.h"
#include "../interface/Direction.h"
#include "AlgorithmRegistry.h"


//using namespace std;

class Sensor;


class RandomChoiceAlgorithm: public AbstractAlgorithm {

	Sensor *sensor;
	AlgorithmConfig config;


public:

	RandomChoiceAlgorithm() {
		cout << "Random Choice Algorithm" << endl;
		srand(time(NULL)); 
	}

	/*virtual ~RandomChoiceAlgorithm(){
		delete (this->robot);
	}*/

    // setSensor is called once when the Algorithm is initialized
	virtual void setSensor(const AbstractSensor& sensor)  override{
    	//this->sensor = (Sensor)sensor; // ?
    }

	// for now..
	void setS (Sensor *sensor){
		this->sensor = sensor;
	}

    // setConfiguration is called once when the Algorithm is initialized
    virtual void setConfiguration(map<string, int> config);

    // step is called by the simulation for each time unit
    virtual Direction step();

    // this method is called by the simulation either when there is a winner or
    // when steps == MaxSteps - MaxStepsAfterWinner
    // parameter stepsTillFinishing == MaxStepsAfterWinner
    virtual void aboutToFinish(int stepsTillFinishing);


};

#endif /* RANDOMCHOICEALGORITHM_H_ */
