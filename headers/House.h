/*
 * House.h
 *
 *  Created on: Mar 15, 2016
 *      Author: anon
 */

#ifndef HOUSE_H_
#define HOUSE_H_

#include <stdio.h>
#include <string>
#include <cstring>
#include <stdlib.h>
#include <algorithm>
#include <iostream>
#include <fstream>
#include "../Errors/ErrorManager.h"

// define a newline character 

#ifdef _WIN32
	#define NEWLINE '\r\n'
#else
	#define NEWLINE '\n'
#endif

#define IS_ZERO(c) (c == '0' || c == ' ')
#define CHAR_VALUE(c) (c >= '0' && c <= '9' ? c - '0' : 0)

using namespace std;

//ErrorManager *em = NULL;

class House {
	
private:
	int rowCount, colCount;
	char **houseLayout = NULL;
	string houseName;
	int dockRow, dockCol;
	int totalDust, dustCleaned;
	int maxSteps;

	static ErrorManager *em;
	//static vector<HouseErrors> hErrs;

public:
	House(){}

	House (House&& h);

	House& operator=(House&& h);

	House& operator=(const House& h);

	virtual ~House();

	bool setHouseLayout(const char *filename);

	void clearHouse();

	static void setErrorManager(ErrorManager* pointHere);

	// checks us location is a wall
	// const because it does not change the house layout
	bool isWall(int row, int col) const {
		return (houseLayout[row][col] == 'W');
	}

	// checks us location is a dock
	// const because it does not change the house layout
	bool isDock(int row, int col) const {
		return (houseLayout[row][col] == 'D');
	}

	// returns dirt level in a location
	// const because it does not change the house layout
	// assumues the requested square is not a wall
	int getDirtLevel(int row, int col) const {
		char square = houseLayout[row][col];
		return CHAR_VALUE(square);
	}

	// returns short name of house
	// const because it does not change the house layout
	string getName() const {
		return houseName;
	}

	int getDockRow() const{
		return dockRow;
	}

	int getDockCol() const{
		return dockCol;
	}

	int getTotalDust() const {
		return totalDust;
	}

	int getDustCleaned() const {
		return dustCleaned;
	}

	int getMaxSteps() const {
		return maxSteps;
	}


	friend ostream& operator<< (ostream& os, const House& h);

	friend ostream& HRprint (ostream& os, const House& h, int x, int y);



	void clean(int row, int col);

	bool isClean();

};


#endif /* HOUSE_H_ */
