/*
 * Simulator.h
 *
 *  Created on: 18 Mar 2016
 *      Author: Liat
 */

#ifndef SIMULATOR_H_
#define SIMULATOR_H_

#include <stdio.h>
#include <iostream>
#include <string>
#include <stdlib.h>
#include <map>
#include <fstream>
#include <vector>

#ifndef _WIN32
#include <dirent.h>
#include <unistd.h>
#endif 

#include "Sensor.h"
#include "House.h"
#include "Battery.h"
#include "../interface/AbstractAlgorithm.h"
#include "../interface/Direction.h"
#include "AlgorithmConfig.h"
#include "Score.h"


using namespace std;

// return codes for running a single step
#define ALG_CONTINUE 0
#define ALG_FINISHED 1
#define ALG_BAD_MOVE 2
#define ALG_BATTERY_DEAD 3

const int DEBUG = 0;

class Simulator {

	vector<AbstractAlgorithm*> algorithms;
	map<AbstractAlgorithm*, Battery> batteryPerAlg;
	map<AbstractAlgorithm*, House> housePerAlg;
	map<AbstractAlgorithm*, pair<int, int>> currLocations;
	map<AbstractAlgorithm*, Score> scorePerAlg;

	Sensor sensor; 
	House currHouse; // holds the current house it simulates the algorithms on
	AlgorithmConfig config;

public:

	Simulator() {}

	// run simulator
	void run();

	// load current house into simulator
	bool loadHouse(string house_path);
	
	void setAlgorithms(vector<AbstractAlgorithm*>& _algs) {
		algorithms = _algs;
	}

	void setSimConfig(map<string, int> _config);

	map<AbstractAlgorithm*, Score>& getAllScores() {
		return scorePerAlg;
	}

	static void debugPrint( Direction d){
		switch (d){
		case Direction::East: cout << "East" << endl; break;
		case Direction::South: cout << "South" << endl; break;
		case Direction::West: cout << "West" << endl; break;
		case Direction::North: cout << "North" << endl; break;
		case Direction::Stay: cout << "Stay" << endl; break;
		}
	}

private:

	// simulate all algorithms on a single house
	void simulateHouseCleaning();

	// after the robot's location is updated, give the sensor info about the dust levels and surrounding walls
	void updateSensor(AbstractAlgorithm* alg);

	int simulateSingleStep(AbstractAlgorithm* alg);

};

#endif /* SIMULATOR_H_ */
