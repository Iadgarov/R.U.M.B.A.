/*
 * Battery.h
 *
 *  Created on: Mar 17, 2016
 *      Author: ubuntu
 */

#ifndef BATTERY_H_
#define BATTERY_H_

#include <algorithm>

class Battery {

	int level;
	int maxLevel;
	int chargeRate;
	int consumptionRate;

public:

	void setBatteryParams(int _maxLevel, int _chargeRate, int _consumptionRate) {
		maxLevel = _maxLevel;
		level = _maxLevel;
		chargeRate = _chargeRate;
		consumptionRate = _consumptionRate;
	}

	int getLevel() {
		return level;
	}

	int getTimeUnitsLeft() {
		return level / consumptionRate;
	}

	void charge() {

		level = min(maxLevel, level + chargeRate);
		//printf("Charging Battery. NewLevel = %d \n", level);
	}

	void consume() {

		level = max(0, level - consumptionRate);
		//printf("Consuming Battery. NewLevel = %d \n", level);
	}

	bool isDead(){
		return (this->level == 0);
	}
	
	void reset() {
		level = maxLevel;
	}
};


#endif /* BATTERY_H_ */
