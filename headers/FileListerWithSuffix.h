#ifndef __FILE_LISTER_WITH_SUFFIX__H_
#define __FILE_LISTER_WITH_SUFFIX__H_

#include "FileLister.h"

class FileListerWithSuffix : public FileLister {

protected:
	string suffix;

	void filterFiles();

	static inline bool endsWith(std::string value, std::string ending);

public:
	FileListerWithSuffix(const string& _basePath, const string& _suffix) : FileLister(_basePath), suffix(_suffix) {
		filterFiles();
	}

	virtual void refresh() {
		FileLister::refresh();
		filterFiles();
	}
	
};

#endif
