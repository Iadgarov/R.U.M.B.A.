#ifndef __SIMULATION_RUNNER__H_
#define __SIMULATION_RUNNER__H_

#include "Simulator.h"
#include "House.h"
#include "Listers.h"
#include "Factory.h"
#include "ScorePrinter.h"
//#include "AlgorithmRegistry.h"
#include <list> 
#include <dlfcn.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string>
#include <cstring>
#include <iostream>

#include "../Errors/ErrorManager.h"

class SimulationRunner {

	HouseLister houseLister;
	AlgorithmLister algLister;
	//list<void *> dl_list; // list to hold handles for dynamic libs 

	map<string, AbstractAlgorithm*> algorithmsByName;
	map<string, int> config;
	map<string, map<string, int>> scoreMatrix; // maps algorithm names to a map of house names to score

	Simulator* simulator = NULL;

	ErrorManager errManager;

public:
	SimulationRunner(HouseLister hlister, AlgorithmLister alister) : houseLister(hlister), algLister(alister){

		errManager.setHouseCount(hlister.length());
		errManager.setAlgorithCount(alister.length());
		AlgorithmError::setPath(alister.getBasePath());
		HouseError::setPath(hlister.getBasePath());
	}

	bool parseConfigurationFile(string filename);

	bool loadSharedObjects();

	bool runSingleHouseSimulation(string house_path);

	void run();

	void initConfig();

	void testHouses(vector<string> *l);

	inline static bool fileExists (const std::string& name) {
	  struct stat buffer;
	  return (stat (name.c_str(), &buffer) == 0);
	}

	~SimulationRunner() {
		delete simulator;
		
		/*for (auto itr = algorithmsByName.begin(); itr != algorithmsByName.end(); itr++) {
			delete itr->second;
			itr->second = NULL;
		}
		
		for (auto hndl : dl_list) {
			dlclose(hndl);
		}*/
	}
};

string getFileName(string filename);

#endif
