#ifndef __FACTORY_H 
#define __FACTORY_H

#include <map> 
#include <string>
#include "../interface/AbstractAlgorithm.h"

// typedef to make it easier to set up our factory 
typedef AbstractAlgorithm *maker_t();
// our global factory 
extern std::map<std::string, maker_t *, std::less<std::string> > factory;

#endif
