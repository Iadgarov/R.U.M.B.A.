#ifndef __LISTERS__H_
#define __LISTERS__H_

#include "FileListerWithSuffix.h"

class HouseLister : public FileListerWithSuffix {

public:
	HouseLister(const string& _basePath) : FileListerWithSuffix(_basePath, ".house") { this->refresh();}

};

class AlgorithmLister : public FileListerWithSuffix {

public:
	AlgorithmLister(const string& _basePath) : FileListerWithSuffix(_basePath, ".so") {this->refresh();}

};

#endif
