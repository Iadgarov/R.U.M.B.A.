#ifndef __FILE_LISTER__H_
#define __FILE_LISTER__H_

#include <iostream>
#include <vector>
#include <algorithm>

#ifndef _WIN32
#include <dirent.h>
#else
#include <windows.h>
#include <Strsafe.h>
#endif

using namespace std;

class FileLister {

protected:
	string basePath;
	vector<string> fileList;

public:
	FileLister(const string& _basePath) : basePath(_basePath) {
		refresh();
	}

	virtual void refresh();

	vector<string> getFileList() {
		return fileList;
	}

	string getBasePath(){
		return basePath;
	}


	int length(){
		return fileList.size();
	}

	//~FileLister();

private:
	static string concatenateAbsolutePath(const string& dirPath, const string& fileName);
};

#endif
