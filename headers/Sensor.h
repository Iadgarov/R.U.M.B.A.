/*
 * Sensor.h
 *
 *  Created on: Mar 15, 2016
 *      Author: anon
 */

#ifndef SENSOR_H_
#define SENSOR_H_

#include "../interface/Direction.h"
#include "../interface/AbstractSensor.h"
#include <iostream>

// defines to help with the isWall array
#define EAST 0
#define WEST 1
#define NORTH 2
#define SOUTH 3

using namespace std;

class Sensor: public AbstractSensor {

	SensorInformation info;

public:

	SensorInformation sense() const override{
		return info;
	}

	void setInfo(bool east, bool west, bool north, bool south, int dLevel){

		this->info.dirtLevel = dLevel;
		this->info.isWall[EAST] = east;
		this->info.isWall[WEST] = west;
		this->info.isWall[NORTH] = north;
		this->info.isWall[SOUTH] = south;

	}

};

#endif /* SENSOR_H_ */
