/*
* Spiral algorithm
* Main idea - stay on every square until it's completely clean
* try to avoid squares the robot has already been on (remember where we went)
* choose next step according to the above and the priority order: east, north, west, south
* return to the docking station when battery is running low (depending on the robot's distance from it)
*/

#ifndef SPIRALALGORITHM_H_
#define SPIRALALGORITHM_H_

#include "AlgorithmConfig.h"
#include <iostream>
#include <map>
#include "Sensor.h"
#include <time.h>
#include <stdlib.h>
#include <stdio.h>

#include "../interface/Direction.h"
#include "Battery.h"
#include "../interface/AbstractAlgorithm.h"
#include "Factory.h"
//#include "AlgorithmRegistry.h"

extern map<string, maker_t *, less<string> > factory;


class SpiralAlgorithm : public AbstractAlgorithm {

	const AbstractSensor* sensor;
	Battery battery;
	AlgorithmConfig config;

	int currRow, currCol; // relative to (0, 0) (the docking station)
	bool finish;
	map<pair<int, int>, char> houseMap; // locations the algorithm is already familiar with. (0, 0) is the docking station
	pair<int, int> prev; // previous square

public:

	SpiralAlgorithm() : currRow(0), currCol(0), finish(false) {}

	virtual void setConfiguration(map<string, int> _config) {
		config.setConfig(_config);
		battery.setBatteryParams(config.BatteryCapacity, config.BatteryRechargeRate, config.BatteryConsumptionRate);
	}

	virtual Direction step();

	virtual void aboutToFinish(int stepsTillFinishing) {
		finish = true;
	}
	
	virtual void setSensor(const AbstractSensor& _sensor);

private:

	virtual int distFromDock();

};

//AUTO_REGISTER_ALGORITHM(SpiralAlgorithm)


#endif /* SPIRALALGORITHM_H_ */
