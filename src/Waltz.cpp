/*
 * RandomChoiseAlgorithm.cpp
 *
 *  Created on: Mar 17, 2016
 *      Author: ubuntu
 */

#include "../headers/Waltz.h"

//using namespace std;

class Simulator;

void Waltz::setConfiguration(map<string, int> config) {
	this->config.MaxStepsAfterWinner = config.at("MaxStepsAfterWinner");
	this->config.BatteryCapacity = config.at("BatteryCapacity");
	this->config.BatteryConsumptionRate = config.at("BatteryConsumptionRate");
	this->config.BatteryRechargeRate = config.at("BatteryRechargeRate");
	this->batteryRemianing = this->config.BatteryCapacity;
}

/*
 * Assumes that simulator sets the info to what it currently is
 */
Direction Waltz::step(){

	SensorInformation info = sensor->sense();
	Direction d = Direction::Stay;

	if ((size_t)timeLeft <= stepHistory.size() || (size_t)batteryRemianing <= ((stepHistory.size() + 1) * config.BatteryConsumptionRate)){
		if (!stepHistory.empty()){
			d = opposite(stepHistory.top());
			stepHistory.pop();
			currentLocation = leadsTo(d);

		}
		if (currentLocation == make_pair(0,0)){
			batteryRemianing = min(batteryRemianing + config.BatteryRechargeRate, config.BatteryCapacity);
			memory.clear(); // if going back to charge erase memory
		}
		else
			batteryRemianing -= config.BatteryConsumptionRate;
		return (d); //backtrack
	}



	// place current location in memory map, we were here!
	const point p = currentLocation; // copy constructor for point
	const pair<point, int> pp = {p, info.dirtLevel};
	memory.insert(pp);

	if (info.dirtLevel > 0){
		batteryRemianing -= config.BatteryConsumptionRate;
		return d; // stay in spot till it is clean
	}


	// if you can charge, charge
	if (currentLocation == make_pair(0,0) && batteryRemianing < config.BatteryCapacity){
		//cout << "Charging" << endl;
		batteryRemianing = min(batteryRemianing + config.BatteryRechargeRate, config.BatteryCapacity);
		return d;
	}


	// else choose first possible direction in order of priority. backtracking lowers priority to last
	for (Direction d : directionPriority){
		if (isMoveLegal(d, info.isWall)){

			//cout << leadsTo(d).first << " , " << leadsTo(d).second ;
			if (memory.find(leadsTo(d)) == memory.end()){
				stepHistory.push(d);
				currentLocation = leadsTo(d);
				if (currentLocation == make_pair(0,0))
					batteryRemianing = min(batteryRemianing + config.BatteryRechargeRate, config.BatteryCapacity);
				else
					batteryRemianing -= config.BatteryConsumptionRate;


				//debugPrint(d);
				return d;
			}
			continue; // take first legal move
		}
	}


	// nothing was chosen, we are either stuck of have been to all spots around us, backtrack if possible, else stay
	if (!stepHistory.empty()){
		d = opposite(stepHistory.top());
		stepHistory.pop();
		currentLocation = leadsTo(d);
		if (currentLocation == make_pair(0,0))
			batteryRemianing = min(batteryRemianing + config.BatteryRechargeRate, config.BatteryCapacity);
		else
			batteryRemianing -= config.BatteryConsumptionRate;
		return (d); //backtrack
	}

	// at this point I have nothing smart to do so just take first possible direction in order of priority
	for (Direction dd : directionPriority){
		if (isMoveLegal(dd, info.isWall)){
			d = dd;
			stepHistory.push(d);
			currentLocation = leadsTo(d);
			break;
		}
	}
	if (currentLocation == make_pair(0,0))
		batteryRemianing = min(batteryRemianing + config.BatteryRechargeRate, config.BatteryCapacity);
	else
		batteryRemianing -= config.BatteryConsumptionRate;
	return d; // nothing is possible, just stay where you are
}

bool Waltz::isMoveLegal( Direction d, bool isWall[4]){
	switch (d){
	case Direction::East: return !isWall[0];
	case Direction::South: return !isWall[3];
	case Direction::West: return !isWall[1];
	case Direction::North: return !isWall[2];
	case Direction::Stay: return true;
	}
	return false;
}

point Waltz::leadsTo (Direction d){
	switch(d){
	case Direction::West: return (make_pair (currentLocation.first - 1, currentLocation.second));
	case Direction:: South: return (make_pair (currentLocation.first, currentLocation.second - 1));
	case Direction::East: return (make_pair (currentLocation.first + 1, currentLocation.second));
	case Direction::North: return (make_pair (currentLocation.first, currentLocation.second + 1));
	case Direction::Stay: return currentLocation;

	}
	return currentLocation;
}

void Waltz::debugPrint( Direction d){
	switch (d){
	case Direction::East: cout << "East" << endl; break;
	case Direction::South: cout << "South" << endl; break;
	case Direction::West: cout << "West" << endl; break;
	case Direction::North: cout << "North" << endl; break;
	case Direction::Stay: cout << "Stay" << endl; break;
	}
}

void Waltz::setSensor(const AbstractSensor& _sensor) {
	sensor = &_sensor;
	emptyStack();
	currentLocation = zero;
	memory.clear();
	timeLeft = INT_MAX;
	batteryRemianing = config.BatteryCapacity;
}

extern "C" AbstractAlgorithm *maker() {
   return new Waltz();
}
