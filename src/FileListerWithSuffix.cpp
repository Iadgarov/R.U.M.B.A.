#include "../headers/FileListerWithSuffix.h"


void FileListerWithSuffix::filterFiles() {
	vector<string> temp = this->fileList;
	this->fileList.clear();
	for (vector<string>::iterator itr = temp.begin(); itr != temp.end(); ++itr) {
		if (endsWith(*itr, this->suffix)) {
			fileList.push_back(*itr);
		}
	}
}

inline bool FileListerWithSuffix::endsWith(string value, string ending) {
	if (ending.size() > value.size()) return false;
	return std::equal(ending.rbegin(), ending.rend(), value.rbegin());
}
