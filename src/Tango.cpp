/*
 * RandomChoiseAlgorithm.cpp
 *
 *  Created on: Mar 17, 2016
 *      Author: ubuntu
 */

#include "../headers/Tango.h"

//using namespace std;

class Simulator;

void Tango::setConfiguration(map<string, int> config) {
	this->config.MaxStepsAfterWinner = config.at("MaxStepsAfterWinner");
	this->config.BatteryCapacity = config.at("BatteryCapacity");
	this->config.BatteryConsumptionRate = config.at("BatteryConsumptionRate");
	this->config.BatteryRechargeRate = config.at("BatteryRechargeRate");
	this->batteryRemianing = this->config.BatteryCapacity;
}

/*
 * Assumes that simulator sets the info to what it currently is
 */
Direction Tango::step(){
	SensorInformation info = (*sensor).sense();
	Direction d = Direction::Stay;

	timeLeft--;

	// place current location in memory map, we were here and saw this!
	memorize(info);

	if (batteryRemianing == config.BatteryCapacity)
		goCharge = false;

	if ((timeLeft <= stepsFromSource.size() ||
			batteryRemianing <= ((stepsFromSource.size() + 1) * config.BatteryConsumptionRate)) || goCharge){

		//cout << "Low battery" << endl;

		if(!goBack)
			returnHere = currentLocation;
		shortestPathTo(zero, currentLocation);
		shortenStack(zero);

		// check again, maybe now stack is short enough for us not to return
		if ((timeLeft <= stepsFromSource.size() ||
				batteryRemianing <= ((stepsFromSource.size() + 1) * config.BatteryConsumptionRate)) || goCharge){

			goCharge = true;
			goBack = true;

			if (!stepsFromSource.empty()){
				d = opposite(stepsFromSource.top());
				stepsFromSource.pop();
				stepsFromFrontLine.push(d);
				currentLocation = leadsTo(d);

			}
			maintainBattery();
			return (d); //backtrack

		}
	}

	// happens after we charge and need to return to where we were working
	if (goBack && returnHere != zero){

		shortestPathTo(returnHere, currentLocation);
		shortenStack(returnHere);

		if (!stepsFromFrontLine.empty()){
			d = opposite(stepsFromFrontLine.top());
			stepsFromFrontLine.pop();
			stepsFromSource.push(d);
			currentLocation = leadsTo(d);
			if (currentLocation == returnHere)
				goBack = false;

		}
		maintainBattery();
		return (d); //backtrack
	}

	if (info.dirtLevel > 0){
		maintainBattery();
		return d; // stay in spot till it is clean
	}

	// else choose first possible direction in order of priority. backtracking lowers priority to last
	for (Direction d : directionPriority){
		if (isMoveLegal(d, info.isWall)){

			//cout << leadsTo(d).first << " , " << leadsTo(d).second ;
			if (beenHere.find(leadsTo(d)) == beenHere.end()){
				stepsFromSource.push(d);
				currentLocation = leadsTo(d);
				maintainBattery();
				return d;
			}
			continue; // take first legal move
		}
	}



	// nothing was chosen, we are either stuck of have been to all spots around us, backtrack if possible, else stay
	if (!stepsFromSource.empty()){

		d = opposite(stepsFromSource.top());
		stepsFromSource.pop();
		currentLocation = leadsTo(d);
		maintainBattery();
		return (d); //backtrack
	}

	// at this point I have nothing smart to do so just take first possible direction in order of priority
	for (Direction dd : directionPriority){
		if (isMoveLegal(dd, info.isWall)){
			d = dd;
			stepsFromSource.push(d);
			currentLocation = leadsTo(d);
			maintainBattery();
			return d;
		}
	}
	maintainBattery();
	return d; // nothing is possible, just stay where you are
}

bool Tango::isMoveLegal( Direction d, bool isWall[4]){
	switch (d){
	case Direction::East: return !isWall[0];
	case Direction::South: return !isWall[3];
	case Direction::West: return !isWall[1];
	case Direction::North: return !isWall[2];
	}
	return false;
}

Point Tango::leadsTo (Direction d){
	switch(d){
	case Direction::West: return (make_pair (currentLocation.first - 1, currentLocation.second));
	case Direction:: South: return (make_pair (currentLocation.first, currentLocation.second - 1));
	case Direction::East: return (make_pair (currentLocation.first + 1, currentLocation.second));
	case Direction::North: return (make_pair (currentLocation.first, currentLocation.second + 1));

	}
	return currentLocation;
}

void Tango::memorize(SensorInformation info){

	if (beenHere.find(currentLocation) == beenHere.end())
		beenHere.insert(make_pair(currentLocation, info.isWall));
	Point temp;
	// mark all reachable locations we know of from this point

	pointData newData;
	newData.point = currentLocation;

	if (seen.find(newData) == seen.end()){
		seen.insert(newData);
	}

	for (int i = 0; i < 4; i++){

		if (!info.isWall[i]){
			switch(i){
			case 0: temp = leadsTo(Direction::East); break;
			case 1: temp = leadsTo(Direction::West); break;
			case 2: temp = leadsTo(Direction::North); break;
			case 3: temp = leadsTo(Direction::South); break;
			}


			pointData lookForMe;
			lookForMe.point = currentLocation;
			auto current = seen.find(lookForMe);

			if ( std::find(current->neighbors.begin(), current->neighbors.end(), temp) == current->neighbors.end()){
				pointData newData;
				newData = *current;
				newData.neighbors.insert(temp);
				seen.erase(current);
				seen.insert(newData);

			}



			lookForMe.point = temp;
			current = seen.find(lookForMe);

			if (current == seen.end()){
				pointData pd;
				pd.point = temp;
				seen.insert(pd); // node with no neighbors other than the source
				current = seen.find(lookForMe);
			}

			if ( std::find(current->neighbors.begin(), current->neighbors.end(), currentLocation) == current->neighbors.end()){
				pointData newData;
				newData = *current;
				newData.neighbors.insert(currentLocation);
				seen.erase(current);
				seen.insert(newData);

			}

		}

	}

}

void Tango::maintainBattery(){
	if (currentLocation == zero)
		batteryRemianing = min(batteryRemianing + config.BatteryRechargeRate, config.BatteryCapacity);
	else
		batteryRemianing -= config.BatteryConsumptionRate;
}

void Tango::shortestPathTo(Point goHere, Point source){

	set<pointData, comp> queue;
	pointData start;

	for (pointData i : seen){
		pointData updated = i;
		updated.distance = ((i.point == source) ? 0 :INT_MAX);
		seen.erase(seen.find(i));
		seen.insert(updated);
		auto t = queue.find(i);
		if (t != queue.end())
			queue.erase(t);
		queue.insert(updated);
	}
	while(!queue.empty()){

		start = *queue.begin();
		queue.erase(queue.begin());

		if (start.point == goHere)
			return;

		int dis = start.distance;
		for (Point t : start.neighbors){

			pointData lookForMe;
			lookForMe.point = t;
			auto next = seen.find(lookForMe);
			if (dis + 1 < next->distance){
				pointData newData;
				newData = *next;
				newData.point = next->point;
				newData.distance = dis + 1;
				newData.prev = start.point;

				seen.erase(next);
				seen.insert(newData);
				auto x = queue.find(lookForMe);
				if (x != queue.end())
					queue.erase(x);
				queue.insert(newData);

			}

		}
	}

}

void Tango::shortenStack(Point start){
	// empty the stack to make room for new shtuff

	stack<Direction> *s = (start == zero) ? &stepsFromSource : &stepsFromFrontLine;
	while(!s->empty())
		s->pop();
	Point next = start;
	Point prev = next;

	//cout << endl << " **** " << endl << "Path back to (" << next.first << "," << next.second << ") in reverse:";
	while (next != currentLocation){
		pointData lookForMe;
		lookForMe.point = next;
		prev = next;
		next = seen.find(lookForMe)->prev;
		if (prev.first - next.first == -1)
			s->push(Direction::East);// cout << "EAST";
		else if (prev.first - next.first == 1)
			s->push(Direction::West);// cout << "WEST ";
		else if (prev.second - next.second == -1)
			s->push(Direction::North);// cout << " NORTH ";
		else if (prev.second - next.second == 1)
			s->push(Direction::South);// cout << "SOUTH ";


	}
	//cout << endl;

}

void Tango::setSensor(const AbstractSensor& _sensor) {
	sensor = &_sensor;
	while(!stepsFromSource.empty())
		stepsFromSource.pop();
	while(!stepsFromFrontLine.empty())
		stepsFromFrontLine.pop();
	currentLocation = zero;
	beenHere.clear();
	seen.clear();
	timeLeft = INT_MAX;
	batteryRemianing = config.BatteryCapacity;
	goBack = false;
	goCharge = false;
	returnHere = zero;
}


extern "C" AbstractAlgorithm *maker() {
   return new Tango();
}

