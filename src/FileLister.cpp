#include "../headers/FileLister.h"

#ifndef _WIN32

void FileLister::refresh() {
	DIR *dir;
	struct dirent *ent;

	fileList.clear();
	if ((dir = opendir(basePath.c_str())) != NULL) {
		/* print all the files and directories within directory */
		while ((ent = readdir(dir)) != NULL) {
			fileList.push_back(concatenateAbsolutePath(basePath, ent->d_name));
		}
		closedir(dir);
	}
	else {
		/* could not open directory
		Remark: IN YOUR CODE, handle this error as well (e.g. throw an exception)... */
		//cout << "Error: could not open directory: " <<  this->basePath_ << endl;
		return;
	}
	std::sort(fileList.begin(), fileList.end());
}

string FileLister::concatenateAbsolutePath(const string& dirPath, const string& fileName) {
	if (dirPath.empty())
		return fileName;
	if (dirPath.back() == '/')
		return dirPath  + fileName;
	return dirPath + "/" + fileName;
}

#else // running on windows

void FileLister::refresh() {
	WIN32_FIND_DATA fileData;
	HANDLE hFind;
	TCHAR path[MAX_PATH];
	
	fileList.clear(); 

	StringCchCopy(path, MAX_PATH, basePath.c_str());
	StringCchCat(path, MAX_PATH, TEXT("\\*"));

	hFind = FindFirstFile(path, &fileData);
	if (hFind != INVALID_HANDLE_VALUE) {
		do {
			fileList.push_back(concatenateAbsolutePath(basePath, fileData.cFileName));
		} while (FindNextFile(hFind, &fileData));

		FindClose(hFind);
	}
	else {
		return;
	}

	std::sort(fileList.begin(), fileList.end());
}

string FileLister::concatenateAbsolutePath(const string& dirPath, const string& fileName) {
	if (dirPath.empty())
		return fileName;
	if (dirPath.back() == '\\')
		return dirPath  + fileName;
	return dirPath + "\\" + fileName;
}



#endif
