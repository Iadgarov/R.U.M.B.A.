#include "../headers/SpiralAlgorithm.h"

// global arrays for the bitmaps. each cell holds the index of the first 0/1 in its position's binary representation
int first1arr[] = {-1, 0, 1, 0, 2, 0, 1, 0, 3, 0, 1, 0, 2, 0, 1, 0};
int first0arr[] = {0, 1, 0, 2, 0, 1, 0, 3, 0, 1, 0, 2, 0, 1, 0, -1};


void SpiralAlgorithm::setSensor(const AbstractSensor& _sensor) {
    sensor = &_sensor;
	
	// reset all member variables
	houseMap.clear();
	houseMap[make_pair(0, 0)] = 'D';
	prev = make_pair(0, 0);
	currRow = 0; currCol = 0;
	finish = false;
	battery.reset();
}

int SpiralAlgorithm::distFromDock() {
	/* returns the (idealized) shortest path distance:
	   row distance + column distance */

	int x = currCol - 0;
	x = (x > 0 ? x : -x);

	int y = currRow - 0;
	y = (y > 0 ? y : -y);

	return x + y;
}

Direction SpiralAlgorithm::step() {

	SensorInformation info = sensor->sense();

	int wallsBmp = 0, cleanBmp = 0; // indicate where there are walls/clean squares
	
	pair<int, int> square(currCol, currRow);

	wallsBmp |= info.isWall[EAST] << EAST;
	wallsBmp |= info.isWall[NORTH] << NORTH;
	wallsBmp |= info.isWall[SOUTH] << SOUTH;
	wallsBmp |= info.isWall[WEST] << WEST;

	if (wallsBmp == 0xf) {
		prev = square;
		return Direction::Stay; // surrounded by walls - nowhere else to go
	}

	if (battery.isDead()) {
		prev = square;
		return Direction::Stay; // stuck
	}

	if (info.dirtLevel > 0) {
		battery.consume();
		prev = square;
		return Direction::Stay; // stay put if current square is not yet clean
	}

	if (currCol == 0 && currRow == 0 && battery.getLevel() < config.BatteryCapacity) {
		battery.charge();
		prev = square;
		return Direction::Stay; // stay put if charging but battery is not fully charged yet
	}

	houseMap[square] = 'C'; // C is for clean, to indicate the robot has already passed here. will be treated (to some extent) as a wall

	/*
	 * TODO: return to docking station if battery is low
	 */
	 if (finish || battery.getTimeUnitsLeft() < distFromDock() * 2) {
	 	// head towards docking station
		 if (currCol == 0 && currRow == 0) {
			 battery.charge();
			 prev = square;
			 return Direction::Stay;
		 }

		 battery.consume(); 

	 	if (0 > currCol && !info.isWall[EAST]) {
			currCol++;
			prev = square;
	 		return Direction::East;
	 	}
	 	if (0 < currCol && !info.isWall[WEST]) {
			currCol--;
			prev = square;
	 		return Direction::West;
	 	}
	 	if (0 > currRow && !info.isWall[SOUTH]) {
			currRow--;
			prev = square;
	 		return Direction::South;
	 	}
	 	if (0 < currRow && !info.isWall[NORTH]) {
			currRow++;
			prev = square;
	 		return Direction::North;
	 	}

		// else - go where we can
		int dir = first0arr[wallsBmp];
		if (dir == EAST) {
			currCol++;
			prev = square;
			return Direction::East;
		}
		if (dir == WEST) {
			currCol--;
			prev = square;
			return Direction::West;
		}
		if (dir == NORTH) {
			currRow++;
			prev = square;
			return Direction::North;
		}
		if (dir == SOUTH) {
			currRow--;
			prev = square;
			return Direction::South;
		}

	 }

	 battery.consume();

	 // clean squares bitmap

	pair<int, int> next(currCol + 1, currRow); // east
	if (houseMap.find(next) != houseMap.end()) {
		cleanBmp |= (houseMap.at(next) == 'C') << EAST;
	}

	next = pair<int, int>(currCol, currRow - 1); // north
	if (houseMap.find(next) != houseMap.end()) {
		cleanBmp |= (houseMap.at(next) == 'C') << NORTH;
	}

	next = pair<int, int>(currCol, currRow + 1); // south
	if (houseMap.find(next) != houseMap.end()) {
		cleanBmp |= (houseMap.at(next) == 'C') << SOUTH;
	}

	next = pair<int, int>(currCol - 1, currRow); // west
	if (houseMap.find(next) != houseMap.end()) {
		cleanBmp |= (houseMap.at(next) == 'C') << WEST;
	}
	
	

	int dir = first0arr[wallsBmp | cleanBmp]; // first unblocked (wall/clean) square
	if (dir == -1) {
		// completely surrounded - choose an arbitrary clean square that isn't prev
		if (prev.first > currCol) {
			cleanBmp &= 0 << EAST;
		}
		else if (prev.first < currCol) {
			cleanBmp &= 0 << WEST;
		}
		else if (prev.second > currRow) {
			cleanBmp &= 0 << SOUTH;
		}
		else if (prev.second < currRow) {
			cleanBmp &= 0 << NORTH;
		}
		
		dir = first1arr[cleanBmp];
	}
	
	prev = square;

	if (dir == EAST) {
		currCol++;
		return Direction::East;
	}
	if (dir == WEST) {
		currCol--;
		return Direction::West;
	}
	if (dir == NORTH) {
		currRow--;
		return Direction::North;
	}
	if (dir == SOUTH) {
		currRow++;
		return Direction::South;
	}

	return Direction::Stay; // shouldn't get here

}


extern "C" AbstractAlgorithm *maker() {
   return new SpiralAlgorithm();
}

