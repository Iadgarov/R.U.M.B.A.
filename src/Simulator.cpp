/*
 * Simulator.cpp
 *
 *  Created on: 18 Mar 2016
 *      Author: Liat
 */

#include "../headers/Simulator.h"

void Simulator::setSimConfig(map<string, int> _config) {
	config.setConfig(_config);
	
	Battery battery;
	battery.setBatteryParams(config.BatteryCapacity, config.BatteryRechargeRate, config.BatteryConsumptionRate);

	for (AbstractAlgorithm* alg : algorithms) {
		batteryPerAlg[alg] = battery;
	}
}

void Simulator::updateSensor(AbstractAlgorithm* alg) {

	int currRow = currLocations[alg].first;
	int currCol = currLocations[alg].second;
//	cout << "row: " << currRow << "\tcol: " << currCol << endl;
	House& house = housePerAlg[alg];

	sensor.setInfo(

			// is there a wall to the right?
			house.isWall(currRow, currCol + 1),

			// is there a wall to the left?
			house.isWall(currRow, currCol - 1),

			// is there a wall to the top?
			house.isWall(currRow - 1, currCol),

			// is there a wall to the bottom?
			house.isWall(currRow + 1, currCol),

			house.getDirtLevel(currRow, currCol)

	);
}

int Simulator::simulateSingleStep(AbstractAlgorithm* alg) {
	int currRow = currLocations[alg].first;
	int currCol = currLocations[alg].second;
	House& house = housePerAlg[alg];
	Battery& battery = batteryPerAlg[alg];

	//cout << &house << endl;

	updateSensor(alg); // update the sensor with the current house info
	Direction d = alg->step();

	int r = currRow, c = currCol;

	if (d == Direction::North) {
		r--;
	}
	else if (d == Direction::South) {
		r++;
	}
	else if (d == Direction::East) {
		c++;
	}
	else if (d == Direction::West) {
		c--;
	}

	currLocations[alg].first = r;
	currLocations[alg].second = c;

	// will print a "animated" cleaning of the house
	if (DEBUG){
		cout << "\033[2J\033[1;1H" << endl;
		HRprint(cout, house, r, c);
		usleep(80000);
	}

	if (house.isWall(r, c)) {
		//cout << "walked into wall" << endl;
		return ALG_BAD_MOVE;
	}

	if (house.isDock(r, c)) {
		//cout << "back at dock" << endl;
		if (house.isClean()) {
			return ALG_FINISHED;
		}
		battery.charge();
	}
	else {
		house.clean(r, c);
		battery.consume();

		if (battery.isDead()) {
			return ALG_BATTERY_DEAD;
		}
	}


	return ALG_CONTINUE; // continue running this algorithm

}

bool Simulator::loadHouse(string house_path) {
	currHouse.clearHouse();
	return currHouse.setHouseLayout(house_path.c_str());
}

void Simulator::run() {

	// setup algorithms, batteries and house copies
	for (AbstractAlgorithm* alg : algorithms) {
		alg->setSensor(sensor);
		batteryPerAlg[alg].setBatteryParams(config.BatteryCapacity, config.BatteryRechargeRate, config.BatteryConsumptionRate);
		housePerAlg[alg] = currHouse;

		currLocations[alg] = make_pair(currHouse.getDockRow(), currHouse.getDockCol());
	}

	int sum_dirt_in_house = currHouse.getTotalDust();

	int winner_num_steps = -1;
	int actual_pos = 1;

	// run all algorithms for MaxSteps steps
	int maxSteps = currHouse.getMaxSteps();
	int steps;
	for (steps = 0; steps < maxSteps; steps++) {
		
		int finished_this_round = 0; // how many algorithms finished this round (to not increment actual_pos again)
		
		if (steps == currHouse.getMaxSteps() - config.MaxStepsAfterWinner) {
			for (AbstractAlgorithm* alg : algorithms) {
				alg->aboutToFinish(config.MaxStepsAfterWinner);
			}
		}


		for (vector<AbstractAlgorithm*>::iterator alg_itr = algorithms.begin(); alg_itr != algorithms.end(); ) {
			int ret = simulateSingleStep(*alg_itr);
			if (ret == ALG_FINISHED) {
				
				finished_this_round++;
				
				if (winner_num_steps == -1) {
					winner_num_steps = steps + 1; // current algorithm is the winner

					for (AbstractAlgorithm* alg : algorithms) {
						alg->aboutToFinish(config.MaxStepsAfterWinner);
					}
					
					maxSteps = min(maxSteps, steps + config.MaxStepsAfterWinner); // update steps bound
				}

				int dirt_collected = housePerAlg[*alg_itr].getDustCleaned();
				scorePerAlg[*alg_itr] = Score(actual_pos, dirt_collected, sum_dirt_in_house, true);
				scorePerAlg[*alg_itr].setParams(winner_num_steps, steps + 1);

				alg_itr = algorithms.erase(alg_itr);
				continue;
			}

			if (ret == ALG_BAD_MOVE) {
			//	cout << "Algorithm did a bad move" << endl;
				scorePerAlg[*alg_itr].setScoreZero();

				alg_itr = algorithms.erase(alg_itr);
				continue;
			}

			if (ret == ALG_BATTERY_DEAD) {
			//	cout << "Algorithm battery dead: " << endl;
				int dirt_collected = housePerAlg[*alg_itr].getDustCleaned();
				scorePerAlg[*alg_itr] = Score(10, dirt_collected, sum_dirt_in_house, false);

				alg_itr = algorithms.erase(alg_itr);
				continue;
			}

			alg_itr++;
		}
		
		actual_pos += finished_this_round; // bump actual_pos to the next place in the competition
	}
	
	if (winner_num_steps == -1) { // if there's no winner
		winner_num_steps = steps;
	}

	// simulation done - check all remaining algorithms
	for (AbstractAlgorithm* alg : algorithms) {
		int dirt_collected = housePerAlg[alg].getDustCleaned();
		scorePerAlg[alg] = Score(10, dirt_collected, sum_dirt_in_house, false);
		scorePerAlg[alg].setParams(winner_num_steps, steps);
	}
	
	for (auto itr = scorePerAlg.begin(); itr != scorePerAlg.end(); itr++) {
		if (itr->second.getScore() == -1) { // only happens for algorithms with dead batteries
			 itr->second.setParams(winner_num_steps, steps);
		}
	}

}

















