#include "../headers/SimulationRunner.h"

#define BUF_SIZE 1024

//map<string, maker_t *, less<string> > factory;

bool SimulationRunner::runSingleHouseSimulation(string house_path) {

	if (!simulator->loadHouse(house_path)) {
		return false;
	}
	simulator->run();

	// add the house scores to the score matrix
	map<AbstractAlgorithm*, Score>& scores = simulator->getAllScores();

	string houseName = getFileName(house_path);

	for (auto itr = algorithmsByName.begin(); itr != algorithmsByName.end(); itr++) {
		string algName = getFileName(itr->first);
		int algScore = scores[itr->second].getScore();

		scoreMatrix[algName][houseName] = algScore;
		//cout << algName << " scored: " << algScore << endl;
	}

	return true;
}

void SimulationRunner::run() {
	// create algorithm vector to pass to the simulator
	vector<AbstractAlgorithm*> algorithms;
	for (auto itr = algorithmsByName.begin(); itr != algorithmsByName.end(); itr++) {
		algorithms.push_back(itr->second);
	}

	simulator = new Simulator();
	simulator->setSimConfig(config);

	vector<string> houseList = houseLister.getFileList();
	if (houseList.empty()){
		errManager.addHouseError(new HouseError(BAD_DIR));
		errManager.fatalError(); // toggle fatal error to positive
	}

	//testHouses(&houseList);
	if (this->errManager.isFatal()){
		this->errManager.printErrors();
		return;
	}
	
	House::setErrorManager(&(this->errManager));

	for (string house : houseList) {
		simulator->setAlgorithms(algorithms);
		runSingleHouseSimulation(house);
			//cout << house << endl;
	}
	
	// final printing:
	ScorePrinter sprinter(scoreMatrix);
	sprinter.print();
	errManager.printErrors();
	
	return;
}

bool SimulationRunner::loadSharedObjects() {
	//list<void *>::iterator itr;
	vector<string> algorithm_names = algLister.getFileList();  // vector of algorithm types used to build menu
	
	if (algorithm_names.empty()){
		errManager.addAlgError(new AlgorithmError(BAD_DIR));
		errManager.fatalError(); // toggle fatal error to positive
	}

	for (string name : algorithm_names) {

		void *hndl = dlopen(name.c_str(), RTLD_NOW);
		//dl_list.push_back(hndl);
    	if (hndl == nullptr) {
    		errManager.addAlgError(new AlgorithmError(BAD_SO, name));
    		continue;
    	}
		
		typedef AbstractAlgorithm* (*maker_t)();

	    maker_t function1 = (maker_t) dlsym(hndl, "maker");
	    if (function1 == nullptr) {
			errManager.addAlgError(new AlgorithmError(BAD_ALG, name));
	    	continue; 	// keep going, just one bad file
	    }
		
	    algorithmsByName[name] = function1();
	    algorithmsByName[name]->setConfiguration(config);
		
	}

	if (this->errManager.isFatal()){
		this->errManager.printErrors();
		return false;
	}

	return true;
}

bool SimulationRunner::parseConfigurationFile(string filename) {

	ifstream fin;
	fin.open(filename.c_str());
	string missingInputs = ""; // pray you never see this used
	string temp, param, value;

	initConfig();


	if (!fin.good()) {
		if (fileExists(filename))
			this->errManager.addConfigError (new ConfigError(filename, NO_OPEN_CONFIG));
		else
			this->errManager.addConfigError(new ConfigError(filename, NO_CONFIG));
		this->errManager.printErrors();
		return false;
	}

	char delimeter = '=';

	while (!fin.eof()) {


		// get the parameter name and value of the current line
		getline(fin, temp);
		param = temp.substr(0, temp.find(delimeter));
		value = temp.substr(temp.find(delimeter) + 1, temp.length());


		auto i = config.find(param);
		// accpet only legal values for config
		if (i != config.end()){
			config[param] = atoi(value.c_str());
		}


	}

	// check that all values were attained
	ConfigError conEr = ConfigError(filename, MISSING); // hopefully will not see use
	for (auto i : config){
		if (i.second == -1){
			conEr.reportMissing(i.first);
		}
	}
	if (conEr.isMissing()){
		cout << conEr.getError(); // skip the manager this time.. why not..
		return false;
	}


	fin.close();
	return true;

}

void SimulationRunner::initConfig(){
	config["MaxStepsAfterWinner"] = -1;
	config["BatteryCapacity"] = -1;
	config["BatteryConsumptionRate"] = -1;
	config["BatteryRechargeRate"] = -1;
}

/*void SimulationRunner::testHouses(vector<string> *l){

	House::setErrorManager(&(this->errManager));
	House temp;

	for (vector<string>::iterator i = (*l).begin(); i != (*l).end(); ){
		if (!temp.setHouseLayout(i->c_str())){
			i = l->erase(i);
		}
		else
			i++;

	}

}*/


// helper method to get the actual file name (no path or suffix)
string getFileName(string filename) {

#ifdef _WIN32
	int start = filename.find_last_of('\\') + 1; // start of the actual file name
#else
	int start = filename.find_last_of('/') + 1; // start of the actual file name
#endif

	int end = filename.find_last_of('.'); // one char past the file name
	return filename.substr(start, end - start);
}
