#include "../headers/ScorePrinter.h"

void ScorePrinter::print() {
	
	cout << dashLine << endl;
	
	// print first row - house names
	cout << '|' << alignRight("", 13); // empty top-left cell
	
	auto alg_itr = scoreMatrix.begin();
	for (auto house_itr = alg_itr->second.begin(); house_itr != alg_itr->second.end(); house_itr++) {
		cout << '|' << alignLeft(house_itr->first, 10); // print house name
	}
	cout << '|' << alignLeft("AVG", 10) << '|' << endl;
	
	cout << dashLine << endl;
	
	// print rest of the score table, a row per algorithm
	for (; alg_itr != scoreMatrix.end(); alg_itr++) {
		cout << '|' << alignLeft(alg_itr->first, 13); // print algorithm name
		
		float avg = 0.0; // keep track of the algorithm scores to calculate average
		
		for (auto house_itr = alg_itr->second.begin(); house_itr != alg_itr->second.end(); house_itr++) {
			avg += house_itr->second;
			cout << '|' << alignRight(to_string(house_itr->second), 10); // print score
		}
		
		stringstream ss;
		ss << fixed << setprecision(2) << (avg / (cols - 2));
		cout << '|' << alignRight(ss.str(), 10) << '|' << endl;
		
		cout << dashLine << endl;
	}
	
}


string ScorePrinter::alignLeft(string name, size_t len) {
	if (name.size() > len - 1) {
		name.resize(len - 1);
	}
	
	name.append(string(len - name.size(), ' ')); // append spaces to get a string of len chars
	return name;
}
	
string ScorePrinter::alignRight(string name, size_t len) {
	string str = string(len - name.size(), ' ');
	str.append(name);
	return str;
}