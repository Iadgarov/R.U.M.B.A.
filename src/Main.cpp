#include "../headers/SimulationRunner.h"

int main(int argc, char *argv[]) {

	string config_path = "";
	string house_path = "";
	string algorithm_path = "";

	for (int i = 1; i < argc; i++) {
		string arg = argv[i];
		//cout << arg << endl;
		if (arg.compare("-config") == 0) {
			config_path.append(argv[i+1]);
			if (config_path.at(config_path.length()-1) != '/') {
				config_path.push_back('/');
			}
			i++;
		}

		else if (arg.compare("-house_path") == 0) {
			house_path.append(argv[i+1]);
			if (house_path.at(house_path.length()-1) != '/') {
				house_path.push_back('/');
			}
			i++;
		}

		else if (arg.compare("-algorithm_path") == 0) {
			algorithm_path.append(argv[i+1]);
			if (algorithm_path.at(algorithm_path.length()-1) != '/') {
				algorithm_path.push_back('/');
			}
			i++;
		}
	}

	if (config_path.length() == 0 ){
		config_path = get_current_dir_name();
		config_path += "/";
	}
	config_path += "config.ini"; // defualt config choice

	if (house_path.length() == 0) {
		house_path = get_current_dir_name();
		house_path += "/";
	}

	if (algorithm_path.length() == 0) {
		algorithm_path = get_current_dir_name();
		algorithm_path += "/";
	}

	
	SimulationRunner runner = SimulationRunner(HouseLister(house_path), AlgorithmLister(algorithm_path));
	if (!runner.parseConfigurationFile(config_path)) {
			return -1;	// config file error, stop run
	}
	if (!runner.loadSharedObjects())
		return -1;
	runner.run();
}
