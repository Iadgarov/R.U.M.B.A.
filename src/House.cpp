/*
 * House.cpp
 *
 *  Created on: Mar 15, 2016
 *      Author: anon
 */

#include "../headers/House.h"

ErrorManager* House::em = NULL;

void House::setErrorManager(ErrorManager* pointHere) {
	em = pointHere;
}

House::House(House&& h){

	cout << "House && stuff" << endl;
	this->rowCount = h.rowCount;
	this->colCount = h.colCount;
	this->houseName = h.houseName;
	this->dockRow = h.dockRow;
	this->dockCol = h.dockCol;
	this->totalDust = h.totalDust;
	this->maxSteps = h.maxSteps;
	// allocate
	this->dustCleaned = h.dustCleaned;
	this->houseLayout = new char*[rowCount];
	for (int r = 0; r < rowCount; r++)
		houseLayout[r] = new char[colCount + 1];

	//copy
	for (int i = 0; i < rowCount; i++){
		for (int j = 0; j < colCount; j++)
			this->houseLayout[i][j] = h.houseLayout[i][j];
	}

	h.houseLayout = NULL; // prevent double freeing of house

}

House& House::operator=(const House& h){

	if (this != &h){

		this->clearHouse();

		this->rowCount = h.rowCount;
		this->colCount = h.colCount;
		this->houseName = h.houseName;
		this->dockRow = h.dockRow;
		this->dockCol = h.dockCol;
		this->totalDust = h.totalDust;
		this->maxSteps = h.maxSteps;
		// allocate
		this->dustCleaned = h.dustCleaned;
		this->houseLayout = new char*[rowCount];
		for (int r = 0; r < rowCount; r++)
			houseLayout[r] = new char[colCount + 1];

		//copy
		for (int i = 0; i < rowCount; i++){
			for (int j = 0; j < colCount; j++)
				this->houseLayout[i][j] = h.houseLayout[i][j];
		}

	}
	return *this;
}

House& House::operator=(House&& h){

	if (this != &h){

		this->clearHouse();

		this->rowCount = h.rowCount;
		this->colCount = h.colCount;
		this->houseName = h.houseName;
		this->dockRow = h.dockRow;
		this->dockCol = h.dockCol;
		this->totalDust = h.totalDust;
		this->maxSteps = h.maxSteps;
		// allocate
		this->dustCleaned = h.dustCleaned;
		this->houseLayout = new char*[rowCount];
		for (int r = 0; r < rowCount; r++)
			houseLayout[r] = new char[colCount + 1];

		//copy
		for (int i = 0; i < rowCount; i++){
			for (int j = 0; j < colCount; j++)
				this->houseLayout[i][j] = h.houseLayout[i][j];
		}

		h.houseLayout = NULL; // prevent double freeing of house

	}
	return *this;
}

void House::clearHouse() {
	if (houseLayout == NULL) {
		return; // no house has been loaded yet
	}

	// free allocated house layout
	for (int r = 0; r < rowCount; r++) {
		delete[] houseLayout[r];
	}
	delete[] houseLayout;

	// reset all house parameters
	houseName.clear();
	rowCount = 0; colCount = 0;
	dockRow = 0; dockCol = 0;
	totalDust = 0; dustCleaned = 0;
	maxSteps = 0;
}

/*
 * sets the house layout and all parameters
 * for now it's just a hard-coded house
 */
bool House::setHouseLayout(const char *filename) {

	ifstream fin;
	fin.open(filename);

	if (!fin.good()) {
		
		em->addHouseError(new HouseError(NO_OPEN_HOUSE, filename));
		return false;
	}

	getline(fin, houseName); // first line - house name
#ifdef _WIN32
	houseName.erase(houseName.length()-1); // remove \r in case of running on windows
#endif

	string line;
	getline(fin, line); // second line - maxSteps on this house
	maxSteps = strtol(line.c_str(), NULL, 10);
	if (maxSteps <= 0) {
		HouseError *he = new HouseError(BAD_LINE, filename);
		he->setBadLine(2, line);
		em->addHouseError(he);
		fin.close();
		return false;
	}

	getline(fin, line); // third line - number of rows
	rowCount = strtol(line.c_str(), NULL, 10);
	if (rowCount <= 0) {
		HouseError *he = new HouseError(BAD_LINE, filename);
		he->setBadLine(3, line);
		em->addHouseError(he);
		fin.close();
		return false;
	}

	getline(fin, line); // fourth line - number of columns
	colCount = strtol(line.c_str(), NULL, 10);
	if (colCount <= 0) {
		HouseError *he = new HouseError(BAD_LINE, filename);
		he->setBadLine(3, line);
		em->addHouseError(he);
		fin.close();
		return false;
	}

	totalDust = 0;
	dustCleaned = 0;

	dockRow = -1;
	dockCol = -1;
	
	bool foundError = false;

	houseLayout = new char*[rowCount];
	for (int r = 0; r < rowCount; r++) {
		
		if (foundError) {
			houseLayout[r] = NULL;
			continue;
		}
		
		houseLayout[r] = new char[colCount + 1];
		string line;

		if (fin.eof()) { // reached end of file - not enough rows
			if (r == 0 || r == rowCount - 1) {
				line = string(colCount, 'W'); // north and south walls
			}
			else {
				line = string(colCount, ' '); // fill row with empty spaces
			}
			strcpy(houseLayout[r], line.c_str());
			continue;
		}

		getline(fin, line);
#ifdef _WIN32
		line.erase(line.length()-1); // remove \r in case of running on windows
#endif

		int len = line.length();

		// if line too short - append empty spaces
		if (len < colCount) { 
			line.append(string(colCount - len, ' '));
		}

		// if line too long - discard extra chars
		else if (len > colCount) {
			line.erase(colCount);
		}

		// make sure east and west walls are acutally walls
		line[0] = 'W';
		line[colCount - 1] = 'W';

		// make sure north and south walls are actually walls
		if (r == 0 || r == rowCount - 1) {
			line = string(colCount, 'W');
		}

		// look for all illegal characters and replace them with empty spaces
		while (true) {
			size_t i = line.find_first_not_of("0123456789 WD");
			if (i == string::npos) {
				break;
			}

			line[i] = ' ';
		}

		// count dust in current row
		for (int i = 0; i < colCount; i++) {
			totalDust += CHAR_VALUE(line[i]);
		}

		strcpy(houseLayout[r], line.c_str());

		// look for docking station in current row
		int c = line.find('D');
		if ((size_t)c == string::npos) {
			continue; // not found
		}

		if (r == 0 || r == rowCount - 1 || c == 0 || c == colCount) {
			continue; // found docking station in the wall - irrelevant
		}

		if (dockRow != -1) {
			em->addHouseError(new HouseError(MANY_D, filename));
			foundError = true;
			continue;
		}

		dockRow = r;
		dockCol = c;

	}

	if (dockRow == -1) {
		em->addHouseError(new HouseError(NO_D, filename));
		foundError = true;
	}

	fin.close();
	return !foundError;

}

/**
 * Assumes we never walk into a wall...
 */
void House::clean(int row, int col) {

	if (houseLayout[row][col] != 'D') { // if current square is not the dock station
		int dirtLevel = getDirtLevel(row, col);
		int newLevel = max(dirtLevel-1, 0);
		this->houseLayout[row][col] = newLevel + '0';

		dustCleaned += dirtLevel - newLevel;
	}


	//printf ("Cleaning location <%d, %d>\n", row, col);

	/*int dirtLevel = getDirtLevel(row, col);
	int newLevel = max(dirtLevel-1, 0);
	sprintf(&(this->houseLayout[row][col]),"%d", newLevel);*/
}


bool House::isClean() {

	return totalDust == dustCleaned;

	/*for (int i = 0; i < rowCount; i++) {
		for (int j = 0; j < colCount; j++) {
			char square = houseLayout[i][j];
			if (square != 'D' && square != 'W' && square != '0') {
				return false;
			}
		}
	}

	return true;*/
}

House::~House() {

	if (this->houseLayout == NULL)
		return;
	for (int i = 0; i < rowCount; i++){
		delete [] houseLayout[i];
		houseLayout[i] = NULL;
	}
	delete this->houseLayout;
	this->houseLayout = NULL;

}


ostream& operator<< (ostream& os,  const House& h){

	os << "House Name: " << h.houseName << endl;

	/*
	os << " | ";

	for (int j = 0; j < h.colCount; j++) {
		printf ("%d | ", j);
	}
	os << endl << "\t============================================" << endl;

	 */

	for (int i = 0; i < h.rowCount; i++) {
		printf (" | ");
		for (int j = 0; j < h.colCount; j++) {

			os << ((h.houseLayout[i][j] == '0') ? ' ' : h.houseLayout[i][j]) << " | ";
		}
		os << endl;// << "\t------------------------------------------" << endl;
	}

	return os;
}

ostream& HRprint (ostream& os, const House& h, int x, int y){

	//os << "House Name: " << h.shortName << endl;
	//os << "Description: " << h.description << endl << endl;

	/*
	os << " | ";

	for (int j = 0; j < h.colCount; j++) {
		printf ("%d | ", j);
	}
	os << endl << "\t============================================" << endl;
	 */

	for (int i = 0; i < h.rowCount; i++) {
		printf (" | ");
		for (int j = 0; j < h.colCount; j++) {
			if (x == i && y == j)
				os << "\u25A0" << " | ";
			else
				os << ((h.houseLayout[i][j] == '0') ? ' ' : h.houseLayout[i][j]) << " | ";
		}
		os << endl;// << "\t------------------------------------------" << endl;
	}

	return os;
}


