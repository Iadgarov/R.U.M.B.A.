/*
 * RandomChoiseAlgorithm.cpp
 *
 *  Created on: Mar 17, 2016
 *      Author: ubuntu
 */

#include "../headers/RandomChoiceAlgorithm.h"

//using namespace std;

class Simulator;

void RandomChoiceAlgorithm::setConfiguration(map<string, int> config) {
	this->config.MaxStepsAfterWinner = config.at("MaxStepsAfterWinner");
	this->config.BatteryCapacity = config.at("BatteryCapacity");
	this->config.BatteryConsumptionRate = config.at("BatteryConsumptionRate");
	this->config.BatteryRechargeRate = config.at("BatteryRechargeRate");
}

/*
 * Assumes that simulator sets the info to what it currently is
 */
Direction RandomChoiceAlgorithm::step(){
	SensorInformation info = (*sensor).sense();
	int next;
	Direction d = Direction::Stay;

	do {
		next = rand() % 5;
	} while (next != 4 && info.isWall[next]); // randomize another direction


	switch (next){
	case 0: d = Direction::East; break;
	case 1: d = Direction::West; break;
	case 2: d = Direction::North; break;
	case 3: d = Direction::South; break;
	case 4: d = Direction::Stay;
	}

	return d;
}

void RandomChoiceAlgorithm::aboutToFinish(int stepsTillFinishing){} // irrelevant for now (?)

extern "C" AbstractAlgorithm *maker() {
   return new RandomChoiceAlgorithm();
}
