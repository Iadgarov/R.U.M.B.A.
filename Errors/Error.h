/*
 * Error.h
 *
 *  Created on: Apr 22, 2016
 *      Author: ubuntu
 */

#ifndef ERROR_H_
#define ERROR_H_

#include <stdio.h>
#include <string>
#include <iostream>
#include <list>
#include <sstream>

using namespace std;

class Error {

	int errorMessage = -1;

	string filename;


public:

	virtual ~Error(){}

	virtual string getError() = 0;

	void setError (int mess){
		this->errorMessage = mess;
	}

	void setFilename( string name){
		filename = name;
	}

	int getErrorMessage(){
		return this->errorMessage;
	}

	string getFilename(){
		int start = filename.find_last_of('/') + 1;
		return filename.substr(start);
	}


};



#endif /* ERROR_H_ */
