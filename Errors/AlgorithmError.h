/*
 * AlgorithmError.h
 *
 *  Created on: Apr 22, 2016
 *      Author: ubuntu
 */

#ifndef ALGORITHMERROR_H_
#define ALGORITHMERROR_H_

#include "Error.h"

#define BAD_DIR 0
#define BAD_SO 	1
#define BAD_ALG 3


class AlgorithmError : public Error {

	static string path;

public:

	~AlgorithmError(){}

	static string getTitle ();

	string getError() override;

	AlgorithmError (int err = -1, string _filename = ""){
		this->setFilename(_filename);
		this->setError(err);
	}

	static void setPath(string p);

};

//string AlgorithmError::path = "";

#endif /* ALGORITHMERROR_H_ */
