#include "AlgorithmError.h"

string AlgorithmError::path = "";

void AlgorithmError::setPath(string p) {
	path = p;
}

string AlgorithmError::getTitle () {
	string returnMe = "All algorithm files in target folder '";
	returnMe.append(path.begin(), path.end());
	string temp = "' cannot be opened or are invalid:\n";
	returnMe.append(temp.begin(), temp.end());
	return returnMe;
}

string AlgorithmError::getError() {
	ostringstream ss;
	switch(this->getErrorMessage()){
	case BAD_DIR: ss << "Bad directory or no algorithm files" << endl; return ss.str(); break;
	case BAD_SO:  ss << (this->getFilename()) << ": " << "file cannot be loaded or is not a valid .so" << endl; return ss.str(); break;
	case BAD_ALG: ss << (this->getFilename()) << ": " << "valid .so but no algorithm was registered after loading it" << endl; return ss.str(); break;
	}
	return "";
}