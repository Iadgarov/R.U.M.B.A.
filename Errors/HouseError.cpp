#include "HouseError.h"

string HouseError::path = "";

void HouseError::setPath(string p){
	path = p;
}

string HouseError::getTitle (){
	string returnMe = "All house files in target folder '";
	returnMe.append(path.begin(), path.end());
	string temp = "' cannot be opened or are invalid:\n";
	returnMe.append(temp.begin(), temp.end());
	return returnMe;
}

// ye sI know I dont need the break. I like it
string HouseError::getError() {
	ostringstream ss;
	switch(this->getErrorMessage()){
	case BAD_DIR: ss << "Bad directory or no house files" << endl; return ss.str(); break;
	case NO_D:	  ss << (this->getFilename()) << ": missing docking station (no D in house)" << endl; return ss.str(); break;
	case MANY_D:  ss <<	(this->getFilename()) << ": too many docking stations (more than one D in house)" << endl; return ss.str(); break;
	case BAD_LINE: ss << (this->getFilename()) << ": line number" << this->line << "in house file shall be a positive number, found: " << this->found << endl; return ss.str(); break;
	case NO_OPEN_HOUSE: ss << (this->getFilename()) << ": cannot open file" << endl; return ss.str(); break;
	}
	
	return ""; // calm down compiler..

}