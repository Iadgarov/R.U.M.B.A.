/*
 * HouseError.h
 *
 *  Created on: Apr 22, 2016
 *      Author: ubuntu
 */

#ifndef HOUSEERROR_H_
#define HOUSEERROR_H_

#include "Error.h"

#define BAD_DIR 0
#define NO_D 	1
#define MANY_D 	2
#define BAD_LINE 3
#define NO_OPEN_HOUSE 4



class HouseError : public Error {

	int line = 0;
	string found;
	static string path;

public:

	~HouseError(){}

	static string getTitle ();

	// ye sI know I dont need the break. I like it
	string getError();

	static void setPath(string p);

	void setBadLine(int _line, string _found){
		this->line = _line;
		this->found = _found;
	}

	HouseError (int err = -1, string _filename = ""){
		this->setError(err);
		this->setFilename(_filename);
	}

};

//string HouseError::path = "";

#endif /* HOUSEERROR_H_ */
