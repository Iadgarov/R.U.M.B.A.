/*
 * ErrorManager.h
 *
 *  Created on: Apr 23, 2016
 *      Author: ubuntu
 */

#ifndef ERRORMANAGER_H_
#define ERRORMANAGER_H_

#include "Error.h"
#include "ConfigError.h"
#include "AlgorithmError.h"
#include "HouseError.h"


class ErrorManager {
	
	list<Error*> algErrors;
	list<Error*> houseErrors;
	Error 		*configError = NULL;
	size_t algorithmCount = 0;
	size_t houseCount = 0;
	bool fError = false;

public:

	~ErrorManager();

	void setAlgorithCount( int t){
		this->algorithmCount = t;
	}

	void setHouseCount( int t){
		this->houseCount = t;
	}

	void fatalError(){
		this->fError = true;
	}

	bool isFatal(){
		return fError;
	}

	void addAlgError (AlgorithmError *e){
		algErrors.push_back(e);
		if (algErrors.size() == algorithmCount)
			fatalError();
	}
	
	void addHouseError (HouseError *e){
		houseErrors.push_back(e);
		if (houseErrors.size() == houseCount)
			fatalError();
	}
	
	void addConfigError (Error *e){
		configError = e;
		fatalError();
	}

	void printErrors ();



};




#endif /* ERRORMANAGER_H_ */
