/*
 * ConfigError.h
 *
 *  Created on: Apr 22, 2016
 *      Author: ubuntu
 */

#ifndef CONFIGERROR_H_
#define CONFIGERROR_H_

#include "Error.h"

#define NO_CONFIG 0
#define NO_OPEN_CONFIG 1
#define MISSING 2

class ConfigError : public Error {

	list<string> missing;
	string path;

public:

	~ConfigError(){}

	void reportMissing(string mia){
		missing.push_back(mia);
	}

	string getError(){
		ostringstream ss;
		switch(this->getErrorMessage()){
		case NO_CONFIG: ss << "config.ini cannot be found" << endl; return ss.str(); break;
		case NO_OPEN_CONFIG: ss << "config.ini exists in '" << this->path << "' but cannot be opened" << endl; return ss.str(); break;
		case MISSING: 	ss << "config.ini missing " << missing.size() << "parameter(s): ";
						if (this->getErrorMessage() == MISSING){
							for (auto i : missing)
								ss << i << " ";
						}
						ss << endl;
						return ss.str();
						break;


		}
		return "";
	}

	bool isMissing(){
		return (missing.size() > 0);
	}

	ConfigError (string _path, int err) : path(_path){
		this->setError(err);
	}
	
};



#endif /* CONFIGERROR_H_ */
