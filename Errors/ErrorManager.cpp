#include "ErrorManager.h"


void ErrorManager::printErrors (){
	if (configError != NULL && configError->getErrorMessage() != -1){
		cout << configError->getError();
		return;
	}

	if (algErrors.size() == algorithmCount){
		cout << AlgorithmError::getTitle();
		for (auto i : algErrors)
			cout << i->getError();
		return;
	}
	
	if (houseErrors.size() == houseCount){
		cout << HouseError::getTitle();
		for (auto i : houseErrors)
			cout << i->getError();
		return;
	}

	cout << endl << "Errors:" << endl;
	for (auto i : houseErrors)
		cout << i->getError();
	for (auto i : algErrors)
		cout << i->getError();

}


ErrorManager::~ErrorManager() {
	for (auto&& e : algErrors) {
		delete e;
	}
	
	for (auto&& e : houseErrors) {
		delete e;
	}
	
	delete configError;
}