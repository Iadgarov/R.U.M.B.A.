/*
 * RandomChoiceAlgorithm.h
 *
 *  Created on: Mar 16, 2016
 *      Author: ubuntu
 */

#ifndef RANDOMCHOICEALGORITHM_H_
#define RANDOMCHOICEALGORITHM_H_

#include "AlgorithmConfig.h"
#include <iostream>
#include <map>
#include "Sensor.h"
#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <stack>
#include <limits.h>
#include <tuple>

#include "../interface/AbstractAlgorithm.h"
#include "../interface/Direction.h"

//using namespace std;

class Sensor;
typedef std::pair<int,int> point;



class Waltz: public AbstractAlgorithm {

	const AbstractSensor* sensor;
	AlgorithmConfig config;
	stack<Direction> stepHistory;	// remember none stay moves to backtrack to dock later
	const Direction directionPriority[5] = {Direction::West, Direction::South, Direction::East, Direction::North, Direction::Stay};
	map<point, int> memory;	// dont really need the value the map saves, may be useful in future..
	point currentLocation;
	int timeLeft = INT_MAX;
	int batteryRemianing;

	const Point zero = make_pair(0,0);

	//const int WALL = -1;
	//const int DOCK = -2;

public:

	Waltz() { 
		currentLocation = zero;
	}

	/*virtual ~RandomChoiceAlgorithm(){
		delete (this->robot);
	}*/

    // setSensor is called once when the Algorithm is initialized
	virtual void setSensor(const AbstractSensor& _sensor);


    // setConfiguration is called once when the Algorithm is initialized
    virtual void setConfiguration(map<string, int> config);

    // step is called by the simulation for each time unit
    virtual Direction step();

    // this method is called by the simulation either when there is a winner or
    // when steps == MaxSteps - MaxStepsAfterWinner
    // parameter stepsTillFinishing == MaxStepsAfterWinner
    virtual void aboutToFinish(int stepsTillFinishing){
    	timeLeft = min(stepsTillFinishing, timeLeft);
    }

    Direction opposite (Direction d){
    	switch(d){
    	case Direction::West: return Direction::East;
    	case Direction:: South: return Direction::North;
    	case Direction::East: return Direction::West;
    	case Direction::North: return Direction::South;
    	case Direction::Stay: return Direction::Stay;

    	}
    	return Direction::Stay;
    }

    bool isMoveLegal(Direction d, bool isWall[4]);

    point leadsTo (Direction d);

    void debugPrint( Direction d);

    void emptyStack(){
    	while(!stepHistory.empty())
    		stepHistory.pop();
    }


};

#endif /* RANDOMCHOICEALGORITHM_H_ */
