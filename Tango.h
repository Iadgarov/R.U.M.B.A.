/*
 * RandomChoiceAlgorithm.h
 *
 *  Created on: Mar 16, 2016
 *      Author: ubuntu
 */

#ifndef TANGO_H_
#define TANGO_H_

#include "AlgorithmConfig.h"
#include <iostream>
#include <map>
#include "Sensor.h"
#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <stack>
#include <tuple>
#include <vector>
#include <limits.h>
#include <queue>
#include <functional>
#include <algorithm>
#include <set>

#include "../interface/AbstractAlgorithm.h"
#include "../interface/Direction.h"

//using namespace std;

class Sensor;


typedef std::pair<int,int> Point;

class pointData{
public:
	Point point;
	int distance = 0;
	Point prev;
	set<Point> neighbors;


	friend bool operator==(pointData a, pointData b){
		return (a.point == b.point);
	}
	friend bool operator<(pointData a, pointData b){
		return (a.point < b.point);
	}
	void operator=(pointData b){
		this->point = b.point;
		this->distance = b.distance;
		this->prev = b.prev;
		this->neighbors = b.neighbors;
	}

};


class comp{
public:
	bool operator()(const pointData a, const pointData b) const{
		if (a.distance < b.distance)
			return true;
		else if (a.distance > b.distance)
			return false;
		else return (a.point < b.point);
	}
};



class Tango: public AbstractAlgorithm {

	const AbstractSensor* sensor;
	AlgorithmConfig 		config;
	stack<Direction> 		stepsFromSource;	// remember none stay moves to backtrack to dock later
	stack<Direction>		stepsFromFrontLine; // use this to return to area we are working on after charge
	const Direction 		directionPriority[5] = {Direction::East, Direction::South, Direction::West, Direction::North, Direction::Stay};
	map<Point, bool*>		beenHere;
	set<pointData> 			seen;
	Point 					currentLocation;
	int 					timeLeft = INT_MAX;
	int 					batteryRemianing;

	bool 					goBack;
	bool					goCharge;
	Point 					returnHere;
	const Point 			zero = make_pair(0,0);




public:

	Tango() {
		currentLocation = zero;
		goBack = false;
		returnHere = zero;
	}

	/*virtual ~RandomChoiceAlgorithm(){
		delete (this->robot);
	}*/

    // setSensor is called once when the Algorithm is initialized
	virtual void setSensor(const AbstractSensor& _sensor);

    // setConfiguration is called once when the Algorithm is initialized
    virtual void setConfiguration(map<string, int> config);

    // step is called by the simulation for each time unit
    virtual Direction step();

    // this method is called by the simulation either when there is a winner or
    // when steps == MaxSteps - MaxStepsAfterWinner
    // parameter stepsTillFinishing == MaxStepsAfterWinner
    virtual void aboutToFinish(int stepsTillFinishing){
    	timeLeft = min(stepsTillFinishing, timeLeft);
    }

    Direction opposite (Direction d){
    	switch(d){
    	case Direction::West: return Direction::East;
    	case Direction:: South: return Direction::North;
    	case Direction::East: return Direction::West;
    	case Direction::North: return Direction::South;

    	}
    	return Direction::Stay;
    }

    bool isMoveLegal(Direction d, bool isWall[4]);

    Point leadsTo (Direction d);

    void shortestPathTo (Point goHere, Point source);

    void memorize(SensorInformation info);

    void maintainBattery();

    void shortenStack(Point start);

  //  point getMinDistanceNodeIndex();


};

#endif
